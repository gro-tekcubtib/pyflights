# pyFlights

#### Python script that:

* Scrapes prices for a given itinary on expedia.com
* Posts the cheapest results to a mongoDB

#### TODO:

* price distribution analysis
* alert via email when prices are low

#### Usage example
`pipenv run python run.py --from-city Chicago, IL, USA --to-city Montreal, QC, Canada --departure-date $DEPARTURE_DATE --return-date $RETURN_DATE`
