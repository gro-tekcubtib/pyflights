'''
Author: Alex-Antoine Fortin
Date: Thursday April 4th 2019
Description
Scrape website to find cheap flights.
Inspiration from:
https://dzone.com/articles/make-python-surf-the-web-for-you-and-send-best-fli
Checklist
[] - Connect Python to our web browser and access the website.
[] - Choose the ticket type based on our preference (round trip, one way, etc.).
[] - Select the departure country.
[] - Select the arrival country (if round trip).
[] - Select departure and return dates.
[] - Compile all available flights in a structured format.
[] - Connect to your email.
[] - Send the best rate for the current hour.
'''
# logging
import logging
logging.basicConfig(level=logging.INFO) # DEBUG, INFO, WARNING, ERROR, CRITICAL

# Surfing the web
from selenium import webdriver
#from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
# Manipulating data
import pandas as pd
# Manipulating dates
import time
from datetime import datetime
# Emailing
import smtplib
from email.mime.multipart import MIMEMultipart

class smallwebdriver(object):
    option = webdriver.ChromeOptions()
    #option.add_argument("--headless")
    browser = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=option)

class route_setup(smallwebdriver):
    def __init__(self, url, departure, arrival, departure_date, return_date):
        # Trip information
        self.url = url
        self.departure = departure
        self.arrival = arrival
        self.departure_date = datetime.strptime(departure_date, '%m/%d/%Y')
        self.return_date = datetime.strptime(return_date, '%m/%d/%Y')

    def _ticket_chooser(self, ticket):
        try:
            ticket_type = self.browser.find_element_by_xpath(ticket)
            ticket_type.click()
        except Exception as e:
            logging.error(f'ticket_type error: {e}')

    def _departure_chooser(self):
        fly_from = self.browser.find_element_by_xpath("//input[@id='flight-origin-hp-flight']"); time.sleep(1)
        fly_from.clear(); time.sleep(0.75)
        fly_from.send_keys('  ' + self.departure); time.sleep(0.5)
        fly_from.send_keys(Keys.DOWN); time.sleep(0.1)
        fly_from.click();
        #first_item = self.browser.find_element_by_xpath("//a[@id='aria-option-0']"); time.sleep(1)
        #first_item.click()

    def _arrival_chooser(self):
        fly_to = self.browser.find_element_by_xpath("//input[@id='flight-destination-hp-flight']"); time.sleep(1)
        fly_to.clear(); time.sleep(0.75)
        fly_to.send_keys('  ' + self.arrival); time.sleep(0.5)
        fly_to.send_keys(Keys.DOWN); time.sleep(0.1)
        fly_to.click();
        #first_item = self.browser.find_element_by_xpath("//a[@id='aria-option-0']"); time.sleep(1)
        #first_item.click()

    def _departure_date_chooser(self):
        dep_date_button = self.browser.find_element_by_xpath("//input[@id='flight-departing-hp-flight']")
        dep_date_button.clear()
        dep_date_button.send_keys('/'.join([self.departure_date.strftime('%m'), self.departure_date.strftime('%d'), self.departure_date.strftime('%Y')]))

    def _return_date_chooser(self):
        return_date_button = self.browser.find_element_by_xpath("//input[@id='flight-returning-hp-flight']")
        for _ in range(11):
            return_date_button.send_keys(Keys.BACKSPACE)
        return_date_button.send_keys('/'.join([self.return_date.strftime('%m'), self.return_date.strftime('%d'), self.return_date.strftime('%Y')]))

    def _search(self):
        '''Define the function that will click the search button.'''
        search = self.browser.find_element_by_xpath("//button[@class='btn-primary btn-action gcw-submit']")
        search.click(); time.sleep(15)

    def run(self):
        colNames = ['ts', 'url','departure', 'departure_date', 'departure_time', 'arrival', 'return_time', 'return_date', 'airline', 'duration', 'stops', 'layovers', 'price']
        colNamesLite = ['ts', 'departure_time', 'return_time', 'airline', 'duration', 'stops', 'layovers', 'price']
        df = pd.DataFrame(columns=colNamesLite)
        # departure times
        dep_times = self.browser.find_elements_by_xpath("//span[@data-test-id='departure-time']")
        dep_times_list = [value.text for value in dep_times]
        # arrival times
        arr_times = self.browser.find_elements_by_xpath("//span[@data-test-id='arrival-time']")
        arr_times_list = [value.text for value in arr_times]
        # airline name
        airlines = self.browser.find_elements_by_xpath("//span[@data-test-id='airline-name']")
        airlines_list = [value.text for value in airlines]
        # prices
        prices = self.browser.find_elements_by_xpath("//span[@data-test-id='listing-price-dollars']")
        price_list = [value.text.split('$')[1] for value in prices]
        #durations
        durations = self.browser.find_elements_by_xpath("//span[@data-test-id='duration']")
        durations_list = [value.text for value in durations]
        #stops
        stops = self.browser.find_elements_by_xpath("//span[@class='number-stops']")
        stops_list = [value.text for value in stops]
        #layovers
        layovers = self.browser.find_elements_by_xpath("//span[@data-test-id='layover-airport-stops']")
        layovers_list = [value.text for value in layovers]
        # Saving query with current date and time
        now = datetime.now()
        current_date = ('-'.join([now.strftime('%Y'), now.strftime('%m'), now.strftime('%d')]))
        current_time = (':'.join([now.strftime('%H'), now.strftime('%M')]))
        ts = ' '.join([current_date, current_time])
        for i, _ in enumerate(dep_times_list):
            try:
                trip_info = [[ts, dep_times_list[i], arr_times_list[i], airlines_list[i], durations_list[i], stops_list[i], layovers_list[i], price_list[i]]]
                trip_info = pd.DataFrame(trip_info, columns=colNamesLite)
            except Exception as e:
                logging.error(f'Error: {e}')
            df = pd.concat([df, trip_info], ignore_index=True, sort=True)
        df['url'] = self.url
        df['departure'] = self.departure
        df['arrival'] = self.arrival
        df['departure_date'] = self.departure_date
        df['return_date'] = self.return_date
        return df[colNames]

    def close_broswer(self):
        try:
            self.browser.close()
            self.browser.quit()
        except Exception as e:
            print(e)

class get_price():
    def __init__(self, url, route_setup, ticket_type, **kwargs):
        self.url = url
        self.ticket_type = ticket_type
        self.route_setup = route_setup(url=url, **kwargs)

    def run(self):
        self.route_setup.browser.get(self.url); time.sleep(6)
        # Choose flights only
        flights_only = self.route_setup.browser.find_element_by_xpath("//button[@id='tab-flight-tab-hp']")
        flights_only.click()

        self.route_setup._ticket_chooser(self.ticket_type)
        self.route_setup._departure_chooser()
        self.route_setup._arrival_chooser()
        self.route_setup._departure_date_chooser()
        self.route_setup._return_date_chooser()
        self.route_setup._search()
        df = self.route_setup.run()
        # Type conversion
        # df = df.infer_objects()
        df['price']=pd.to_numeric(df.price.apply(lambda x: x.replace(',', '')))
        self.route_setup.close_broswer()
        return df
