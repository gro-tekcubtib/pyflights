'''
Author: Alex-Antoine Fortin
Date: Saturday, April 6th 2019
Description
Utility tools to communicate with a mongoDB
'''

from pymongo import MongoClient
from werkzeug.utils import import_string

# class reading config file
class app(object):
    config = {}
    for key in dir(import_string('config')):
        if key.isupper():
            config[key] = getattr(import_string('config'), key)

# MONGODB utility functions
def _connect_mongo(conf):
    """ A util for making a connection to mongo """
    if conf['DATABASE_USERNAME'] and conf['DATABASE_PASSWORD']:
        mongo_uri = 'mongodb://{}:{}@{}:{}/{}'.format(conf['DATABASE_USERNAME'], conf['DATABASE_PASSWORD'], conf['DATABASE_ADDRESS'], conf['DATABASE_PORT'], conf['DATABASE_NAME'])
        conn = MongoClient(mongo_uri)
    else:
        conn = MongoClient(conf['DATABASE_ADDRESS'], conf['DATABASE_PORT'])
    return conn[conf['DATABASE_NAME']]

def read_mongo(db, collection, key='', value=''):
    """ Read from Mongo and Store into DataFrame """
    # Make a query on key value to the specific DB and Collection
    cursor = db[collection].aggregate([{"$match": {key: value}}])
    # Expand the cursor and return all hits for the 'id' in the database
    myentries = list(cursor)
    return myentries

def insert_mongo(db, collection, dict_to_insert):
    db[collection].insert_one(dict_to_insert)
