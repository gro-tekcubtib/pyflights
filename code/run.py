'''
Author: Alex-Antoine Fortin
Date: Thursday April 4th 2019
Description
Scrape website to find cheap flights.
Inspiration from:
https://dzone.com/articles/make-python-surf-the-web-for-you-and-send-best-fli
Checklist
[] - Connect Python to our web browser and access the website.
[] - Choose the ticket type based on our preference (round trip, one way, etc.).
[] - Select the departure country.
[] - Select the arrival country (if round trip).
[] - Select departure and return dates.
[] - Compile all available flights in a structured format.
[] - Connect to your email.
[] - Send the best rate for the current hour.
'''
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--url', type=str, default='https://www.expedia.com/', help='Currently supported: https://www.expedia.com/')
parser.add_argument('--from-city', type=str, nargs='+', help="Str. Where you want to fly from.")
parser.add_argument('--to-city', type=str, nargs='+', help="Str. Where you want to fly to.")
parser.add_argument('--departure-date', type=str, help="Str. Date that you want to go.")
parser.add_argument('--return-date', type=str, help="Str. Date that you want to fly back.")
args = parser.parse_args()

import time
from tools import smallwebdriver, route_setup, get_price
import mongoUtils as mongo
from mongoUtils import app

# Setting ticket types paths
return_ticket = "//label[@id='flight-type-roundtrip-label-hp-flight']"
one_way_ticket = "//label[@id='flight-type-one-way-label-hp-flight']"
multi_ticket = "//label[@id='flight-type-multi-dest-label-hp-flight']"

if __name__ == "__main__":
    results = get_price(url=args.url,
                        route_setup=route_setup,
                        ticket_type=return_ticket,
                        departure=' '.join(args.from_city),
                        arrival=' '.join(args.to_city),
                        departure_date=args.departure_date,
                        return_date=args.return_date).run()

    print(f'from-city: {" ".join(args.from_city)}')
    print(f'to-city: {" ".join(args.to_city)}')
    print(f'departure-date: {" ".join(args.departure_date)}')
    print(f'return-date: {" ".join(args.return_date)}')

    print(results.sort_values(by='price').head(5))

    # Posting to mongo
    db = mongo._connect_mongo(app.config)
    for rows in results.sort_values(by='price').head(5).iterrows():
        mongo.insert_mongo(db, app.config['DATABASE_COLLECTION'], dict(rows[1]))

"""
class args():
    url = 'https://www.expedia.com/'
    from_city = ['Montreal,', 'QC,', 'Canada']
    to_city = ['Milwaukee,', 'WI,', 'USA']
    departure_date = '06/21/2019'
    return_date = '07/21/2019'
"""
